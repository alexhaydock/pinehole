#!/bin/bash
set -euf -o pipefail

# Some content from GitHub Gist here:
#   https://gist.githubusercontent.com/Syquel/9c070f15672162c2951a32c497f95ffc/raw/c9cc7c69f4db9486aa326f5f0f66a7c8961489d7/unbound_block_list_generator.sh

# readonly LAST_MODIFIED_MARKER_SUFFIX=".lastmodified"
readonly AWK_HOSTS_BUILDER_BASE=$(cat <<"EOT"
BEGIN { split(WHITELIST, whitelist); for (i in whitelist) whitedict[tolower(whitelist[i])]++ }
{ sub(/\r$/, "", $NF); $2 = tolower($2) }
NF==2 && $2 && !/^#/ && !($2 in whitedict) && !visited[$2]++
EOT
)

# Allow us to choose between returning actual replies (0.0.0.0 and ::0) or returning NXDOMAIN
readonly AWK_HOSTS_BUILDER_NULL='{ print "local-zone: \"" $2 "\" redirect\nlocal-data: \"" $2 " A 0.0.0.0\"\nlocal-data: \"" $2 " AAAA ::0\"" }'
readonly AWK_HOSTS_BUILDER_NX='{ print "local-zone: \"" $2 "\" always_nxdomain" }'

# Declare our whitelist
#
# There's a thread on the Pi-Hole site for commonly-whitelisted domains that otherwise might break things
#   https://discourse.pi-hole.net/t/commonly-whitelisted-domains/212
#
# events.splunk.com      - Splunk events domain. Not sure why this ended up on a blocklist - maybe for advertising?
# identity.mparticle.com - Breaks McDonalds App
# learn.cisecurity.org   - Seems to be blocked for some reason
# links.zoopla.co.uk     - Used by Zoopla for click tracking but also breaks the app/emails
# mparticle.com          - Without this, we can't resolve identity.mparticle.com
# t.co                   - Outbound tracking redirect for Twitter, but breaks basically every link on Twitter if we block it
# vms.boldchat.com       - Breaks live support chat on some sites including Three UK
declare -a WHITELIST=(
  "events.splunk.com"
  "identity.mparticle.com"
  "learn.cisecurity.org"
  "links.zoopla.co.uk"
  "mparticle.com"
  "t.co"
  "vms.boldchat.com"
  )

# I'm trying to use only well-trusted lists here and not go too crazy.
# It seems like currently (Apr 2022) the StevenBlack file is the only one that Pi-Hole actually uses
# by default.
#
# NOTE: I've deprecated the BadNews list, but re-adding the line below would re-enable it:
# [MyCustomList_badnews]="https://gitlab.com/alexhaydock/pinehole/-/raw/main/badnews.txt"
declare -A HOSTS_URLS=(
  [AdAway]="https://adaway.org/hosts.txt"
  [MyCustomList_d3ward]="https://gitlab.com/alexhaydock/pinehole/-/raw/main/d3ward.txt"
  [NoTracking]="https://raw.githubusercontent.com/notracking/hosts-blocklists/master/hostnames.txt"
  [SomeoneWhoCares]="https://someonewhocares.org/hosts/zero/hosts"
  [StevenBlack]="https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts"
  [URLhaus]="https://urlhaus.abuse.ch/downloads/hostfile/"
  [YoyoInternetServices]="https://pgl.yoyo.org/as/serverlist.php?showintro=0&hostformat=hosts&mimetype=plaintext"
)

RAW_HOSTS=""
for HOSTS_KEY in "${!HOSTS_URLS[@]}"; do
  HOSTS_URL="${HOSTS_URLS[${HOSTS_KEY}]}"
  echo "Processing ${HOSTS_KEY} ${HOSTS_URL}"
  RAW_HOSTS+=$(wget --compression=auto --header="Content-Type: text/plain" -qO- "${HOSTS_URL}") 
  RAW_HOSTS+=$'\n'
done

TMP_FILE="$(mktemp)"
TARGET_FILE="adblock.list"

# This is where we can configure whether to use our NULL hosts builder, that returns 0.0.0.0 and ::0, or to
# use our NX hosts builder, that returns NXDOMAIN
AWK_HOSTS_BUILDER="${AWK_HOSTS_BUILDER_BASE} ${AWK_HOSTS_BUILDER_NX}"

echo "Writing temporary hosts file to ${TMP_FILE}"
echo "${RAW_HOSTS}" | awk -v WHITELIST="${WHITELIST[*]}" "${AWK_HOSTS_BUILDER}" | sort -t: -k2 | uniq > "${TMP_FILE}"

echo "Moving hosts file to ${TARGET_FILE}"
mv "${TMP_FILE}" "${TARGET_FILE}"
